#[cfg(test)]
mod tests {

    use assert_cmd::prelude::*; // Add methods on commands
    use assert_cmd::Command as AssertCommand;
    use predicates::prelude::*; // Used for writing assertions
    use std::process::Command; // Run programs

    #[test]
    fn test_input_file_doesnt_exist() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("qoi")?;

        cmd.arg("--input-file").arg("test/file/doesnt/exist");
        cmd.arg("--output-file").arg("output.png");
        cmd.assert()
            .failure()
            .stderr(predicate::str::contains("No such file or directory"));

        Ok(())
    }

    #[test]
    fn test_decode_rgba() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("qoi")?;

        cmd.arg("--input-file").arg("tests/test-input-rgba.qoi");
        cmd.arg("--output-file").arg("tests/output.png");
        cmd.assert().success();

        Ok(())
    }

    #[test]
    fn test_decode_rgb() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = Command::cargo_bin("qoi")?;

        cmd.arg("--input-file").arg("tests/test-input-rgb.qoi");
        cmd.arg("--output-file").arg("tests/output.png");
        cmd.assert().success();

        Ok(())
    }

    #[test]
    fn test_decode_stdin_stdout() -> Result<(), Box<dyn std::error::Error>> {
        let mut cmd = AssertCommand::cargo_bin("qoi")?;

        cmd.arg("--input-file").arg("-");
        cmd.arg("--output-file").arg("-");
        let _pipe_result = cmd.pipe_stdin("tests/test-input-rgb.qoi");
        let _result = cmd.output();
        cmd.assert().success();

        Ok(())
    }
}
