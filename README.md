## Overview
qoi-rs is a [QOI](https://qoiformat.org/) image decoder which uses [nom](https://docs.rs/nom/latest/nom/) for parsing. It currently outputs only to PNG.

### Run it

```sh
cargo run -- -i input.qoi -o output.png
```

or with `stdin` and `stdout`:

```sh
cargo run -- -i - -o - > output.png < input.qoi
```

### Run tests

```sh
cargo test
```

### Fuzz testing

Initial setup:

```sh
cargo install cargo-fuzz
```

List fuzzing targets:

```sh
cargo fuzz list
```

Run fuzzing target:

```sh
cargo fuzz run decode_target
```

### Benchmarking

```sh
cargo bench --features=bench
```

### Roadmap

- [x] Split into separate files
- [x] Benchmarks
- [x] Fuzzing
- [ ] Output to other formats
- [ ] Add QOI encoding

### Credits

Huge thanks to @lu-zero for giving feedback on both Nom and Rust ergonomics.
