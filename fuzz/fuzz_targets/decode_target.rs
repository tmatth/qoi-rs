#![no_main]

use libfuzzer_sys::fuzz_target;
use qoi_rs::{decode, parse_header};

fuzz_target!(|data: &[u8]| {
    let Ok((chunks, header)) = parse_header(&data) else { return };
    let Ok((_leftover, (_pixels, _leftover_px))) = decode(chunks, header.width, header.height) else { return };
});
