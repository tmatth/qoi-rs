use std::fs::File;
use std::io::{stdin, BufReader, Read};
use std::iter;
use std::num::Wrapping;

use rgb::RGBA8;

pub struct QOIHeader {
    pub width: u32,
    pub height: u32,
    pub channels: u8,
    pub colorspace: u8,
}

struct RGBDiff {
    pub dr: i8, // [-2..1] r channel difference from the previous pixel
    pub dg: i8, // [-2..1] g channel difference from the previous pixel
    pub db: i8, // [-2..1] b channel difference from the previous pixel
}

struct LumaDiff {
    pub dg: i8,    // [-32..31] 6-bit green channel difference from previous pixel
    pub dr_dg: i8, // [-8..7] 4-bit red channel difference minus green channel difference
    pub db_dg: i8, // [-8..7] 4-bit blue channel difference minus green channel difference
}

// FIXME: this is just an artifical constraint from the reference implementation
pub const QOI_MAX_PIXELS: u32 = 32_000_000u32;

const QOI_OP_RGB_TAG: u8 = 0b11111110;
const QOI_OP_RGBA_TAG: u8 = 0b11111111;
const QOI_OP_INDEX_TAG: u8 = 0b00;
const QOI_OP_DIFF_TAG: u8 = 0b01;
const QOI_OP_LUMA_TAG: u8 = 0b10;
const QOI_OP_RUN_TAG: u8 = 0b11;

const QOI_BYTE_STREAM_END_TAG: [u8; 8] = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01];

fn index_hash(px: RGBA8) -> usize {
    ((px.r as u32 * 3 + px.g as u32 * 5 + px.b as u32 * 7 + px.a as u32 * 11) % 64) as usize
}

fn apply_diff(px: &RGBA8, diff: &RGBDiff) -> RGBA8 {
    RGBA8 {
        r: (Wrapping(px.r as i8) + Wrapping(diff.dr)).0 as u8,
        g: (Wrapping(px.g as i8) + Wrapping(diff.dg)).0 as u8,
        b: (Wrapping(px.b as i8) + Wrapping(diff.db)).0 as u8,
        a: px.a,
    }
}

fn apply_luma_diff(px: &RGBA8, diff: &LumaDiff) -> RGBA8 {
    RGBA8 {
        r: (Wrapping(px.r as i8) + Wrapping(diff.dg + diff.dr_dg)).0 as u8,
        g: (Wrapping(px.g as i8) + Wrapping(diff.dg)).0 as u8,
        b: (Wrapping(px.b as i8) + Wrapping(diff.dg + diff.db_dg)).0 as u8,
        a: px.a,
    }
}

mod nom_impl {
    use super::*;
    use nom::{
        bits::bits,
        bits::complete::{tag as bits_tag, take as bits_take},
        branch::alt,
        bytes::complete::tag,
        combinator::{map, verify},
        multi::many_till,
        number::streaming::{be_u32, be_u8},
        sequence::{preceded, tuple},
        IResult,
    };

    fn parse_size(qoi_buffer: &[u8]) -> IResult<&[u8], (u32, u32)> {
        verify(tuple((be_u32, be_u32)), |(x, y)| {
            x > &0u32 && y > &0u32 && ((QOI_MAX_PIXELS / *x) > *y)
        })(qoi_buffer)
    }

    fn parse_color_info(qoi_buffer: &[u8]) -> IResult<&[u8], (u8, u8)> {
        verify(tuple((be_u8, be_u8)), |(ch, csp)| {
            (ch == &3u8 || ch == &4u8) && (csp == &0u8 || csp == &1u8)
        })(qoi_buffer)
    }

    // https://qoiformat.org/qoi-specification.pdf
    pub fn parse_header(qoi_buffer: &[u8]) -> IResult<&[u8], QOIHeader> {
        map(
            preceded(tag("qoif"), tuple((parse_size, parse_color_info))),
            |((width, height), (channels, colorspace))| QOIHeader {
                width,      // image width in pixels (BE)
                height,     // image height in pixels (BE)
                channels,   // 3 = RGB, 4 = RGBA
                colorspace, // 0 = sRGB with linear alpha | 1 = all channels linear
            },
        )(qoi_buffer)
    }

    // See: https://adamchalmers.com/nom-bits/
    type BitInput<'a> = (&'a [u8], usize);

    // This is just a simple wrapper around the `tag` parser, but it makes the
    // parameter types concrete instead of generic, so now Rust knows how to actually
    // store the pattern.
    fn tag_parser(pattern: u8, count: u8, input: BitInput) -> IResult<BitInput, u8> {
        bits_tag(pattern, count)(input)
    }

    fn take_n_bits(count: usize, input: BitInput) -> IResult<BitInput, u8> {
        bits_take(count)(input)
    }

    fn parse_rgb(qoi_buffer: &[u8]) -> IResult<&[u8], RGBA8> {
        let rgb_tag_parser = bits(|input| tag_parser(QOI_OP_RGB_TAG, 8, input));
        let rgb_parser = tuple((be_u8, be_u8, be_u8));

        map(preceded(rgb_tag_parser, rgb_parser), |(r, g, b)| RGBA8 {
            r,
            g,
            b,
            a: 255, /* alpha value remains unchanged from previous pixel */
        })(qoi_buffer)
    }

    fn parse_rgba(qoi_buffer: &[u8]) -> IResult<&[u8], RGBA8> {
        let rgba_tag_parser = bits(|input| tag_parser(QOI_OP_RGBA_TAG, 8, input));
        let rgba_parser = tuple((be_u8, be_u8, be_u8, be_u8));

        map(preceded(rgba_tag_parser, rgba_parser), |(r, g, b, a)| {
            RGBA8 { r, g, b, a }
        })(qoi_buffer)
    }

    fn parse_index(qoi_buffer: &[u8]) -> IResult<&[u8], u8> {
        let index_tag_parser = |input| tag_parser(QOI_OP_INDEX_TAG, 2, input);
        let index_parser = |input| take_n_bits(6usize, input);

        bits(move |input| preceded(index_tag_parser, index_parser)(input))(qoi_buffer)
    }

    fn parse_diff(qoi_buffer: &[u8]) -> IResult<&[u8], RGBDiff> {
        let diff_tag_parser = |input| tag_parser(QOI_OP_DIFF_TAG, 2, input);
        let take2bits = |input| take_n_bits(2usize, input);
        let diff_parser = move |input| tuple((take2bits, take2bits, take2bits))(input);

        map(
            bits(move |input| preceded(diff_tag_parser, diff_parser)(input)),
            |(dr, dg, db)| RGBDiff {
                dr: dr as i8 - 2,
                dg: dg as i8 - 2,
                db: db as i8 - 2,
            },
        )(qoi_buffer)
    }

    fn parse_luma_diff(qoi_buffer: &[u8]) -> IResult<&[u8], LumaDiff> {
        let luma_tag_parser = |input| tag_parser(QOI_OP_LUMA_TAG, 2, input);
        let take6bits = |input| take_n_bits(6usize, input);
        let take4bits = |input| take_n_bits(4usize, input);
        let luma_parser = move |input| tuple((take6bits, take4bits, take4bits))(input);

        map(
            bits(move |input| preceded(luma_tag_parser, luma_parser)(input)),
            |(dg, dr_dg, db_dg)| LumaDiff {
                dg: dg as i8 - 32,
                dr_dg: dr_dg as i8 - 8,
                db_dg: db_dg as i8 - 8,
            },
        )(qoi_buffer)
    }

    fn parse_run(qoi_buffer: &[u8]) -> IResult<&[u8], u8> {
        let run_tag_parser = |input| tag_parser(QOI_OP_RUN_TAG, 2, input);
        let run_parser = |input| take_n_bits(6usize, input);

        map(
            bits(move |input| preceded(run_tag_parser, run_parser)(input)),
            |run_length| run_length + 1,
        )(qoi_buffer)
    }

    enum QOIChunk {
        Rgb(RGBA8),
        Rgba(RGBA8),
        Index(u8),
        Diff(RGBDiff),
        LumaDiff(LumaDiff),
        RunLength(u8),
    }

    pub fn decode_path(path: String) -> (QOIHeader, Vec<RGBA8>) {
        let mut buffer = Vec::new();

        if path == "-" {
            // Read stdin
            stdin().read_to_end(&mut buffer).unwrap();
        } else {
            // Read file
            let f = File::open(path).unwrap();
            let mut reader = BufReader::new(f);
            reader.read_to_end(&mut buffer).unwrap();
        }

        let Ok((chunks, header)) = parse_header(&buffer) else {
            todo!() // grcov-excl-line
        };

        let Ok((_leftover, (pixels, _leftover_px))) = decode(chunks, header.width, header.height)
        else {
            todo!() // grcov-excl-line
        };

        assert!(pixels.len() == (header.width * header.height) as usize);

        (header, pixels)
    }

    pub fn decode(
        qoi_buffer: &[u8],
        width: u32,
        height: u32,
    ) -> IResult<&[u8], (Vec<RGBA8>, &[u8])> {
        map(
            many_till(
                alt((
                    map(parse_rgb, QOIChunk::Rgb),
                    map(parse_rgba, QOIChunk::Rgba),
                    map(parse_index, QOIChunk::Index),
                    map(parse_diff, QOIChunk::Diff),
                    map(parse_luma_diff, QOIChunk::LumaDiff),
                    map(parse_run, QOIChunk::RunLength),
                )),
                tag(QOI_BYTE_STREAM_END_TAG),
            ),
            // Given the parsed chunks, write pixels
            |(data, input)| {
                let mut previous_pixel = RGBA8 {
                    r: 0,
                    g: 0,
                    b: 0,
                    a: 255,
                };

                let mut previously_seen = [previous_pixel; 64];

                // Already validated by parse_header
                debug_assert!(width != 0 && height != 0);

                // Already validated by parse_header
                debug_assert!(height < (QOI_MAX_PIXELS / width));

                let mut result = Vec::with_capacity((width * height) as usize);

                for chunk in data.iter() {
                    match chunk {
                        QOIChunk::Rgb(px) => {
                            // The alpha value remains unchanged from previous pixel.
                            let pixel = RGBA8 {
                                r: px.r,
                                g: px.g,
                                b: px.b,
                                a: previous_pixel.a,
                            };
                            result.push(pixel);
                            previous_pixel = pixel;
                            previously_seen[index_hash(pixel)] = pixel;
                        }
                        QOIChunk::Rgba(px) => {
                            result.push(*px);
                            previous_pixel = *px;
                            previously_seen[index_hash(*px)] = *px;
                        }
                        QOIChunk::Index(index) => {
                            let px = previously_seen[*index as usize];
                            result.push(px);
                            previous_pixel = px;
                            previously_seen[index_hash(px)] = px;
                        }
                        QOIChunk::Diff(diff) => {
                            let px = apply_diff(&previous_pixel, diff);
                            result.push(px);
                            previous_pixel = px;
                            previously_seen[index_hash(px)] = px;
                        }
                        QOIChunk::LumaDiff(diff) => {
                            let px = apply_luma_diff(&previous_pixel, diff);
                            result.push(px);
                            previous_pixel = px;
                            previously_seen[index_hash(px)] = px;
                        }
                        QOIChunk::RunLength(run_length) => {
                            result.extend(iter::repeat(previous_pixel).take((*run_length).into()));
                        }
                    }
                }
                (result, input)
            },
        )(qoi_buffer)
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_parse_header() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x00,
            ];
            let Ok((_input, header)) = parse_header(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(
                header.width == 1247
                    && header.height == 713
                    && header.channels == 4
                    && header.colorspace == 0
            )
        }

        #[test]
        fn test_parse_invalid_magic() {
            let input: [u8; 14] = [
                0x72, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x00,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Invalid header found");
                return;
            };
        }

        #[test]
        fn test_parse_header_width_too_small() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x00,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Width of 0");
                return;
            };
            unreachable!();
        }

        #[test]
        fn test_parse_header_height_too_small() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Height of 0");
                return;
            };
            unreachable!();
        }

        #[test]
        fn test_parse_header_dims_too_large() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x04, 0x00,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Size exceeds {}", QOI_MAX_PIXELS);
                return;
            };
            unreachable!();
        }

        #[test]
        fn test_parse_invalid_channels() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x05, 0x00,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Invalid channels, expected 3 or 4");
                return;
            };
            unreachable!();
        }

        #[test]
        fn test_parse_invalid_colorspace() {
            let input: [u8; 14] = [
                0x71, 0x6f, 0x69, 0x66, 0x00, 0x00, 0x04, 0xdf, 0x00, 0x00, 0x02, 0xc9, 0x04, 0x02,
            ];
            let Ok((_input, _header)) = parse_header(&input) else {
                eprintln!("Invalid colorspace, expected 0 or 1");
                return;
            };
            unreachable!();
        }

        #[test]
        fn test_parse_rgb() {
            let input = [0b1111_1110, 0xaa, 0xbb, 0xcc];
            let Ok((_input, rgb)) = parse_rgb(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(rgb.r == 0xaa && rgb.g == 0xbb && rgb.b == 0xcc && rgb.a == 0xff)
        }

        #[test]
        fn test_parse_rgba() {
            let input = [0b1111_1111, 0xaa, 0xbb, 0xcc, 0xdd];
            let Ok((_input, rgba)) = parse_rgba(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(rgba.r == 0xaa && rgba.g == 0xbb && rgba.b == 0xcc && rgba.a == 0xdd)
        }

        #[test]
        fn test_index_hash() {
            assert!(
                index_hash(RGBA8 {
                    r: 1,
                    g: 2,
                    b: 3,
                    a: 4
                }) == 14
            )
        }

        #[test]
        fn test_parse_index() {
            let input = [0b00_00_00_10];
            let Ok((_input, index)) = parse_index(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(index == 0x02)
        }

        #[test]
        fn test_parse_diff() {
            let input: [u8; 1] = [0b01_00_10_11u8];
            let Ok((_input, drgb)) = parse_diff(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(drgb.dr == -2);
            assert!(drgb.dg == 0);
            assert!(drgb.db == 1);
        }

        #[test]
        fn test_apply_diff() {
            let neg_wrap_result = apply_diff(
                &RGBA8 {
                    r: 1,
                    g: 1,
                    b: 1,
                    a: 1,
                },
                &RGBDiff {
                    dr: -2,
                    dg: -2,
                    db: -2,
                },
            );
            assert!(neg_wrap_result.r == 255);
            assert!(neg_wrap_result.g == 255);
            assert!(neg_wrap_result.b == 255);
            assert!(neg_wrap_result.a == 1); /* unchanged */

            let pos_wrap_result = apply_diff(
                &RGBA8 {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 255,
                },
                &RGBDiff {
                    dr: 1,
                    dg: 1,
                    db: 1,
                },
            );
            assert!(pos_wrap_result.r == 0);
            assert!(pos_wrap_result.g == 0);
            assert!(pos_wrap_result.b == 0);
            assert!(pos_wrap_result.a == 255); /* unchanged */
        }

        #[test]
        fn test_parse_luma_diff() {
            let input: [u8; 2] = [0b10_000000u8, 0b0000_1111u8];
            let Ok((_input, dluma)) = parse_luma_diff(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(dluma.dg == -32);
            assert!(dluma.dr_dg == -8);
            assert!(dluma.db_dg == 7);
        }

        #[test]
        fn test_apply_luma_diff() {
            let neg_wrap_result = apply_luma_diff(
                &RGBA8 {
                    r: 10,
                    g: 10,
                    b: 10,
                    a: 10,
                },
                &LumaDiff {
                    dg: -13,
                    dr_dg: -13,
                    db_dg: -13,
                },
            );
            assert!(neg_wrap_result.r == 240);
            assert!(neg_wrap_result.g == 253);
            assert!(neg_wrap_result.b == 240);
            assert!(neg_wrap_result.a == 10); /* unchanged */

            let pos_wrap_result = apply_luma_diff(
                &RGBA8 {
                    r: 250,
                    g: 250,
                    b: 250,
                    a: 250,
                },
                &LumaDiff {
                    dg: 7,
                    dr_dg: 7,
                    db_dg: 7,
                },
            );
            assert!(pos_wrap_result.r == 8);
            assert!(pos_wrap_result.g == 1);
            assert!(pos_wrap_result.b == 8);
            assert!(pos_wrap_result.a == 250); /* unchanged */
        }

        #[test]
        fn test_parse_run() {
            // parse 2 runs
            let input: [u8; 2] = [0b11_000000u8, 0b11_111101u8];
            let Ok((rest, run)) = parse_run(&input) else {
                todo!() // grcov-excl-line
            };
            assert!(run == 1);
            let Ok((_input, run)) = parse_run(&rest) else {
                todo!() // grcov-excl-line
            };
            assert!(run == 62);
        }

        #[test]
        fn test_decode_all_chunk_types() {
            let input: [u8; 22] = [
                0b1111_1110u8, /* QOI_OP_RGB */
                0xffu8,
                0xffu8,
                0xffu8,
                0b1111_1111u8, /* QOI_OP_RGBA */
                0xffu8,
                0xffu8,
                0xffu8,
                0xffu8,
                0b00_111111u8, /* QOI_OP_INDEX */
                0b01_111111u8, /* QOI_OP_DIFF */
                0b10_111111u8, /* QOI_OP_LUMA */
                0xffu8,
                0b1111_1000u8, /* QOI_OP_RUN */
                0x0u8,         /* QOI_BYTE_STREAM_END_TAG */
                0x0u8,
                0x0u8,
                0x0u8,
                0x0u8,
                0x0u8,
                0x0u8,
                0x01u8,
            ];
            /* This isn't a 1x1 image, we just need placeholders. */
            let Ok((_, (_, _))) = decode(&input, 1, 1) else {
                todo!() // grcov-excl-line
            };
        }
    }
}

pub use nom_impl::decode;
pub use nom_impl::decode_path;
pub use nom_impl::parse_header;
