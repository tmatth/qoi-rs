use std::io::{stdout, Result, Write};
// for debug output
use std::fs::File;
use std::io::BufWriter;

use clap::Parser;

use qoi_rs::decode_path;
use rgb::{ComponentBytes, RGB8};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the QOI input file
    #[arg(short, long)]
    input_file: String, // grcov-excl-line

    /// Name of the PNG output file
    #[arg(short, long)]
    output_file: String, // grcov-excl-line
}

fn save_output(path: &str, width: u32, height: u32, channels: u8, pixels: &[u8]) {
    let w: BufWriter<Box<dyn Write>> = match path {
        "-" => {
            // write to stdout
            BufWriter::new(Box::new(stdout()))
        }
        _ => {
            // write to file for any other path
            let file = File::create(path).unwrap();
            BufWriter::new(Box::new(file))
        }
    };

    let mut encoder = png::Encoder::new(w, width, height);
    if channels == 4 {
        encoder.set_color(png::ColorType::Rgba);
    } else {
        encoder.set_color(png::ColorType::Rgb);
    }
    encoder.set_depth(png::BitDepth::Eight);
    let mut writer = encoder.write_header().unwrap();

    writer.write_image_data(pixels).unwrap();
}

fn main() -> Result<()> {
    let args = Args::parse();

    let (header, pixels) = decode_path(args.input_file);

    assert!(pixels.len() == (header.width * header.height) as usize);

    if header.channels == 3 {
        // Just convert our RGBA output to RGB if the input QOI was originally RGB
        let rgb_pixels: Vec<RGB8> = pixels
            .into_iter()
            .map(|px| RGB8 {
                r: px.r,
                g: px.g,
                b: px.b,
            })
            .collect();

        assert!(rgb_pixels.len() == (header.width * header.height) as usize);

        save_output(
            &args.output_file,
            header.width,
            header.height,
            header.channels,
            rgb_pixels.as_bytes(),
        );
    } else {
        save_output(
            &args.output_file,
            header.width,
            header.height,
            header.channels,
            pixels.as_bytes(),
        );
    }

    Ok(())
}

#[cfg(test)]
mod tests {

    use crate::save_output;
    use rgb::{ComponentBytes, RGBA8};

    #[test]
    fn test_save_output() {
        let mut buffer = Vec::with_capacity((1 * 1) as usize);
        buffer.push(RGBA8 {
            r: 1,
            g: 2,
            b: 3,
            a: 4,
        });
        save_output("/dev/null", 1, 1, 4, buffer.as_bytes());
    }
}
