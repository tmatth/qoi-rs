use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};

use qoi_rs::{decode, decode_path, parse_header};

#[cfg(feature = "bench")]
mod c {
    use std::ffi::{c_int, c_void, CString};

    #[repr(C)]
    #[derive(Debug, Copy, Clone)]
    pub struct qoi_desc {
        pub width: ::std::os::raw::c_uint,
        pub height: ::std::os::raw::c_uint,
        pub channels: ::std::os::raw::c_uchar,
        pub colorspace: ::std::os::raw::c_uchar,
    }

    extern "C" {
        pub fn qoi_decode(
            data: *const ::std::os::raw::c_void,
            size: ::std::os::raw::c_int,
            desc: *mut qoi_desc,
            channels: ::std::os::raw::c_int,
        ) -> *mut ::std::os::raw::c_void;

        pub fn qoi_read(
            filename: *const ::std::os::raw::c_char,
            desc: *mut qoi_desc,
            channels: ::std::os::raw::c_int,
        ) -> *mut ::std::os::raw::c_void;
    }

    pub(crate) fn decode_qoi_c() {
        let mut desc = qoi_desc {
            width: 1247,
            height: 713,
            channels: 4,
            colorspace: 0,
        };

        unsafe {
            let desc_ptr = &mut desc; // raw pointer

            // make the call, convert and then own
            // the returned C string
            let _ = qoi_decode(
                super::INPUT.as_ptr() as *const c_void,
                super::INPUT.len() as c_int,
                desc_ptr,
                4,
            );
        }
    }
    pub(crate) fn decode_qoi_files_c() {
        let mut desc = qoi_desc {
            width: 0,
            height: 0,
            channels: 0,
            colorspace: 0,
        };

        let c_string = CString::new("benches/clovisfest.qoi").expect("Cstring::new failed");
        unsafe {
            let _pixels = qoi_read(c_string.as_ptr(), &mut desc, 4);
        }
    }
}

static INPUT: [u8; 36] = [
    0x71,
    0x6f,
    0x69,
    0x66,
    0x00,
    0x00,
    0x04,
    0xdf,
    0x00,
    0x00,
    0x02,
    0xc9,
    0x04,
    0x00,
    0b1111_1110u8, /* QOI_OP_RGB */
    0xffu8,
    0xffu8,
    0xffu8,
    0b1111_1111u8, /* QOI_OP_RGBA */
    0xffu8,
    0xffu8,
    0xffu8,
    0xffu8,
    0b00_111111u8, /* QOI_OP_INDEX */
    0b01_111111u8, /* QOI_OP_DIFF */
    0b10_111111u8, /* QOI_OP_LUMA */
    0xffu8,
    0b1111_1000u8, /* QOI_OP_RUN */
    0x0u8,         /* QOI_BYTE_STREAM_END_TAG */
    0x0u8,
    0x0u8,
    0x0u8,
    0x0u8,
    0x0u8,
    0x0u8,
    0x01u8,
];

fn decode_qoi_rs() {
    let Ok((chunks, header)) = parse_header(&INPUT) else {
        todo!()
    };
    let Ok((_, (_, _))) = decode(&chunks, header.width, header.height) else {
        todo!()
    };
}

fn decode_qoi_files_rs() {
    let (_header, _pixels) =
        // original: https://commons.wikimedia.org/wiki/File%3AClovisfest.jpg
        // (Creative Commons Attribution-Share Alike 3.0 Unported)
        decode_path("benches/clovisfest.qoi".to_string());
}

fn bench_decoders(c: &mut Criterion) {
    let mut group = c.benchmark_group("Decode memory");
    for i in [0].iter() {
        group.bench_with_input(BenchmarkId::new("qoi-rs", i), i, |b, _| {
            b.iter(|| decode_qoi_rs())
        });
        #[cfg(feature = "bench")]
        group.bench_with_input(BenchmarkId::new("qoi-c", i), i, |b, _| {
            b.iter(|| c::decode_qoi_c())
        });
    }
    group.finish();

    let mut group = c.benchmark_group("Decode files");
    for i in [0].iter() {
        group.bench_with_input(BenchmarkId::new("qoi-files-rs", i), i, |b, _| {
            b.iter(|| decode_qoi_files_rs())
        });
        #[cfg(feature = "bench")]
        group.bench_with_input(BenchmarkId::new("qoi-files-c", i), i, |b, _| {
            b.iter(|| c::decode_qoi_files_c())
        });
    }
    group.finish();
}

criterion_group!(benches, bench_decoders);
criterion_main!(benches);
