// build.rs

fn main() {
    #[cfg(feature = "bench")]
    cc::Build::new()
        .file("benches/qoi-wrapper.c")
        .opt_level(3)
        .define("QOI_IMPLEMENTATION", None)
        .compile("qoi-wrapper");
}
